<?php
/*
 * Фабричный метод (англ. Factory Method) — порождающий шаблон проектирования, предоставляющий подклассам интерфейс для
 *  создания экземпляров некоторого класса. В момент создания наследники могут определить, какой класс создавать. Иными
 *  словами, Фабрика делегирует создание объектов наследникам родительского класса. Это позволяет использовать в коде
 *  программы не специфические классы, а манипулировать абстрактными объектами на более высоком уровне.
 *  Также известен под названием виртуальный конструктор.
 */

interface Product
{
    public function encode();
}

interface Creator
{
    public function getProduct();
}

class FirstProduct implements Product
{
    public function encode()
    {
        echo __CLASS__;
    }
}

class SecondProduct implements Product
{
    public function encode()
    {
        echo __CLASS__;
    }
}

class FirstCreator implements Creator
{
    public function getProduct()
    {
        return new FirstProduct();
    }
}

class SecondCreator implements Creator
{
    public function getProduct()
    {
        return new SecondProduct();
    }
}

$firstCreator = new FirstCreator();
echo $firstCreator->getProduct()->encode(); //FirstProduct

$secondCreator = new SecondCreator();
echo $secondCreator->getProduct()->encode(); //SecondProduct